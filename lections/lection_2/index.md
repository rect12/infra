## HTTPS

**Препятствия** 

1. Скорость на сервере
2. Скорость на клиенте
3. Поддержка https другими сервисами (http в iframe)
4. Затраты и сложность

**Пример**

[httpvshttps.com]()

**Стоимость**

1. Покупка сертификата
2. Обновление сертификата
3. Повторение каждый год
4. Затраты времени это тоже стоимость

Как использовать HTTPS?

- Если есть доступ к командной строке сервера - [certbot.com]()

- Если нет доступа - использовать хостинг с Lets Encrypt


## Терминология

1. **SNI** - Server Name Indication -  Множественные сертификаты на одном сервере
2. **SAN** - Subject Alternative Name - Множественные домены на одном сертификате
3. **PFS** - Perfect Forward Secrecy - В случае, если приватный ключ был скомпрометирован, предыдущие сессии с использованием этого ключа не будут скомпрометированы
4. **DNSSEC** - Domain Name System Security Extensions
5. **DANE** - DNS bases Aithentication of Named entities
6. **CAA** - Certificate Authotiry Authorisation
7. **CRL** = Certificate Revocation list
8. **PKP** - Public Key Pinning

[HTTP Public key pinning](https://developer.mozilla.org/ru/docs/Web/HTTP/Public_Key_Pinning)
[Extended SSL Cerificate](https://www.digicert.com/extended-validation-ssl.htm)
[Google Brotly](https://github.com/google/brotli)

