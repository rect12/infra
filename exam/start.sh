#!/bin/bash

# enable logging
set -x
exec > >(tee /var/log/user-data.log|logger -t user-data ) 2>&1

# installing puppet
wget http://apt.puppetlabs.com/puppetlabs-release-trusty.deb
sudo dpkg -i puppetlabs-release-trusty.deb
sudo apt-get update
sudo apt-get install puppet puppet-common -y

# configuring puppet
sudo rm /etc/puppet/puppet.conf
sudo touch /etc/puppet/puppet.conf
sudo chmod 0777 /etc/puppet/puppet.conf
sudo cat > /etc/puppet/puppet.conf <<-EOF
[main]
logdir=/var/log/puppet
vardir=/var/lib/puppet
ssldir=/var/lib/puppet/ssl
rundir=/var/run/puppet
factpath=$confdir/facter
EOF

# configure server
sudo touch /etc/puppet/manifests/site.pp
sudo chmod 0777 /etc/puppet/manifests/site.pp
sudo cat > /etc/puppet/manifests/site.pp <<-EOF
exec { 'first_server':
  command => 'while true; do echo "HTTP/1.1 200 OK\r\n\r\n{\"health\": \"ok\"}" | nc -l 8080; done &',
  provider => 'shell'
}

exec { 'second_server':
  command => 'while true; do echo "HTTP/1.1 200 OK\r\n\r\nhello world" | nc -l 9090; done &',
  provider => 'shell'
}
EOF

# run puppet
puppet apply /etc/puppet/manifests/site.pp
