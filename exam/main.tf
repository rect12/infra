variable "first_port" {
  description = "Server port for HTTP requests."
  default = 8080
}
variable "second_port" {
  description = "Server port for HTTP requests."
  default = 9090
}

data "local_file" "start_script" {
  filename = "start.sh"
}

provider "aws" {
  region = "eu-west-1"
}

resource "aws_instance" "puppet" {
  ami = "ami-0c59fb6cd1d16a1ce"
  instance_type = "t2.micro"
  user_data = "${data.local_file.start_script.content}"

  tags {
    Name = "puppet"
  }

  vpc_security_group_ids = [
    "${aws_security_group.instance.id}"
  ]

  key_name = "lazuka23_key"
}

resource "aws_security_group" "instance" {
  name = "puppet-security-group"

  # first server
  ingress {
    from_port = "${var.first_port}"
    to_port = "${var.first_port}"
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  # second server
  ingress {
    from_port = "${var.second_port}"
    to_port = "${var.second_port}"
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  # allow ssh
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}